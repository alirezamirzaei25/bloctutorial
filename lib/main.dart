import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertutorial/BlocDelegate.dart';
import 'package:fluttertutorial/bloc/test_bloc.dart';
import 'package:fluttertutorial/testingbloc.dart';

void main() {
  BlocSupervisor.delegate = AppBlocDelegate();

  runApp(
      MultiBlocProvider(providers: [

        BlocProvider<TestBloc>(create: (context) => TestBloc(),),
        
      ], child: MaterialApp(
          home: TestingPage()),)
  );
}




//https://api.koobook.app/Patoghs/
