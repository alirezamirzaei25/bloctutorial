import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertutorial/bloc/test_bloc.dart';
import 'package:fluttertutorial/bloc/test_event.dart';
import 'package:fluttertutorial/bloc/test_state.dart';


class TestingPage extends StatefulWidget {
  @override
  _TestingPageState createState() => _TestingPageState();
}

class _TestingPageState extends State<TestingPage> {


  TestBloc  testBloc;

  @override
  void initState() {
    super.initState();
    testBloc = BlocProvider.of<TestBloc>(context);


  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);


    return WillPopScope(
      child: Scaffold(

        body:
        Center(
            child: BlocBuilder<TestBloc, TestState>(
              builder: (context, state) {
                if (state is state2) {
                  return Text('state2');
                } else if (state is state1) {
                  return Column(
                    children: [


                      FlatButton(onPressed: (){
                        testBloc.add(FromState1ToState2());
                      }, child: Text('change state'),color: Colors.orange,),

                      FlatButton(onPressed: (){
                        testBloc.add(getLocation());
                      }, child: Text('show Number Of Location '),color: Colors.blue,),

                    ],
                  );
                }
                else if (state is loading)
                  {
                    return Text('loading');
                  }
                else if(state is showNumberOfLocation)
                  {
                    return Text('number of Location : ${state.numberOfLocation}');
                  }
                else
                {
                  return Text('error');
                }

              },
            )
        ),
      ),
    );
  }





}