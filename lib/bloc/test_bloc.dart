import 'dart:async';
import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertutorial/bloc/test_event.dart';
import 'package:fluttertutorial/bloc/test_state.dart';



class TestBloc extends Bloc<TestEvent, TestState> {


  @override
  TestState get initialState => state1();

  @override
  Stream<TestState> mapEventToState(TestEvent event) async* {
    if (event is FromState1ToState2) {
      try{
        yield loading();
//        await function1();
        yield state2();
      }
      catch(e)
      {
        error();
      }

    }
    else if (event is getLocation)
      {
        yield showNumberOfLocation(event.numberOfLocation);
      }
    else{
      yield loading();
    }
  }


  Stream<TestState> _mapLoggedOutToState() async* {
    yield loading();
  }
}

//
//void function1 () async
//{
//
//
//  for(int i = 0 ; i<1000 ; i++)
//    {
//     print(i);
//    }
//
//}
