

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';






abstract class TestState extends Equatable {
  const TestState();

  @override
  List<Object> get props => [];
}





class loading extends TestState {

}


class error extends TestState {
}

class showNumberOfLocation extends TestState {
  int numberOfLocation = 0;

  showNumberOfLocation(this.numberOfLocation);


}



class state1 extends TestState {
}


class state2 extends TestState {
}